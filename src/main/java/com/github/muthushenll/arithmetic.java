package com.github.muthushenll;

import java.math.BigDecimal;

/**
 * Created by Mk on 3/4/2019.
 */

/**
 * Really silly class that provides the same functionality as {@link BigDecimal}
 * itself for simple arithmetic operationsXD
 */

public class arithmetic {

    public static BigDecimal add(BigDecimal x, BigDecimal y) {
        return x.add(y);
    }

    public static BigDecimal subtract(BigDecimal x, BigDecimal y) {
        return x.subtract(y);
    }

    public static BigDecimal multiply(BigDecimal x, BigDecimal y) {
        return x.multiply(y);
    }

    public static BigDecimal divide(BigDecimal x, BigDecimal y) {
        return x.divide(y);
    }
}
